import pymysql
import json

def lambda_handler(event, context):
	# Connect to the database.	
	connection = pymysql.connect(host='192.168.5.134', user='root', password='1234',db='rajustech')
	try:
		with connection.cursor() as cursor:
		# Create a new record
		sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
		cursor.execute(sql, ('webmaster@python.org', 'very-secret'))

		# connection is not autocommit by default. So you must commit to save
		# your changes.
		connection.commit()

		with connection.cursor() as cursor:
		# Read a single record
		sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
		cursor.execute(sql, ('webmaster@python.org',))
		result = cursor.fetchone()
		print(result)

		return "Hello from Lambda"
	finally:
		connection.close()	