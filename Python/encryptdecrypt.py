# https://gitlab.com/raju.sreenivasan/awslambda
import base64

def encode(key, clear):
	enc = []
	for i in range(len(clear)):
		key_c = key[i % len(key)]		
		#print(key_c)
		enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)		
		#print(enc_c)
		enc.append(enc_c)	
	#print(str(enc))
	base64.urlsafe_b64encode("".join(enc))
	return base64.urlsafe_b64encode("".join(enc))
	
def decode(key, enc):
	dec = []
	enc = base64.urlsafe_b64decode(enc)
	print(range(len(enc)))
	for i in range(len(enc)):
		key_c = key[i % len(key)]		
		dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
		dec.append(dec_c)
	return "".join(dec)

if __name__ == "__main__":	
	encryptedUserName = encode("123$%^&",'root')
	UserName = decode("123$%^&", "o6GimA==")
	print(encryptedUserName)
	print(UserName)