/* Simple Hello World in Node.js */
console.log("Hello World");

encryptedPassword = encrypt('password')
decryptedPassword = decrypt('dca5f25814f855d42cd5a415e7a817d9')

console.log(encryptedPassword)
console.log(decryptedPassword)

function encrypt(data) {
  try {
    var crypto = require('crypto');
    var cipher= crypto.createCipher('aes-128-cbc', 'mypassword')  
    var output = cipher.update(data, 'utf8', 'hex') + cipher.final('hex')
    return output;
  }
  catch(exception) {
    console.log(exception.message);
  }
}

function decrypt(data) {
  try {
    var crypto = require('crypto');
    var cipher= crypto.createDecipher('aes-128-cbc', 'mypassword');
    var output = cipher.update(data, 'hex', 'utf8') + cipher.final('utf8');
    return output;
  }
  catch(exception) {
    console.log(exception.message);
  }
}